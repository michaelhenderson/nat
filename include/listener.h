/* listener.h - Listener is base class for interface listeners
 *
 * This listener provides functions that are used by a thread
 * that listens on any interface of the NAT device.
 *
 * Copyright 2007 by Valentin Koch and Michael Henderson
 */
#ifndef LISTENER_H
#define LISTENER_H

#include "nattable.h"

/** \class Listener
 *
 *  structure containing the data for the Listener base class
 */
typedef struct listener_str* Listener;

/** create_listener
 *
 *  Listener constructor
 *
 *  \return Listener object or NULL
 */
Listener create_listener();

/** delete_listener
 *
 *  Listener destructor
 *
 *  WARNING:  You must call delete_listener to destroy the object
 *            or you will leak memory.
 *  \param this the Listener object to destroy
 */
void delete_listener(Listener this);


/** listen_iface
 *
 *  Starts listening on the specified interface 
 *  \param this  Listener object
 *  \param if_in  interface to listen on
 *  \param if_out interface to inject translated packets
 *  \param child_listener  pointer to the child listener object
 *  \param l_pipe_in  pipe descriptor to send data to the translator
 *  \param l_pipe_out pipe descriptor to receive data from the translator
 *  \param name  the name of the interface (ie trusted or untrusted)
 */
void listen_iface(Listener this, 
		char* if_in, 
		char* if_out, 
		void* child_listener, 
		int l_pipe_in, 
		int l_pipe_out,
		char* name);

#endif
