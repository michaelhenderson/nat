/* trlistener.h - Listener for the Trust Interface
 *
 * This listener provides functions that are used by a thread
 * that listens on the trust interface of the NAT device.
 *
 * Copyright 2007 by Valentin Koch and Michael Henderson
 */
#ifndef TRLISTENER_H
#define TRLISTENER_H

#include "listener.h"

/** \class TRListener
 *
 *  structure containing the data for the TRListener child class
 *  parent is Listener
 */
typedef struct trlistener_str* TRListener; 

/** create_tr_listener
 *
 *  TRListener constructor
 *
 *  \return TRListener object or NULL
 */
TRListener create_tr_listener();

/** delete_tr_listener
 *
 *  TRListener destructor
 *
 *  WARNING:  You must call delete_tr_listener to destroy the object
 *            or you will leak memory.
 *  \param this the TRListener object to destroy
 */
void delete_tr_listener(TRListener this);

/** tr_listen
 *
 *  main function of the trusted interface listener process
 *  \param this  TRListener object
 *  \param iface_in  interface to listen on
 *  \param iface_out interface to inject packets
 */
void tr_listen(TRListener this, char* iface_in, char* iface_out);

#endif
