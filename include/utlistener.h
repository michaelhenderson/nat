/* utlistener.h - Listener for the Untrust Interface
 *
 * This listener provides functions that are used by a thread
 * that listens on the untrust interface of the NAT device.
 *
 * Copyright 2007 by Valentin Koch and Michael Henderson
 */
#ifndef UTLISTENER_H
#define UTLISTENER_H

#include "listener.h"

/** \class UTListener
 *
 *  structure containing the data for the UTListener child class
 *  parent is Listener
 */
typedef struct utlistener_str* UTListener;

/** create_ut_listener
 *
 *  UTListener constructor
 *
 *  \return UTListener object or NULL
 */
UTListener create_ut_listener();

/** delete_ut_listener
 *
 *  UTListener destructor
 *
 *  WARNING:  You must call delete_ut_listener to destroy the object
 *            or you will leak memory.
 *  \param this the UTListener object to destroy
 */
void delete_ut_listener(UTListener this);

/** ut_listen
 *
 *  main function of the untrusted interface listener process
 *  \param this  UTListener object
 *  \param iface_in  interface to listen on
 *  \param iface_out interface to inject packets
 */
void ut_listen(UTListener this, char* iface_in, char* iface_out);

#endif
