/*

nat - Network Address Translator
Copyright (C) 2007  Michael Henderson and Valentin Koch

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA


*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include "nat.h"
#include "nattable.h"

/** Oct macro
 * Used to help display IP address in dotted decimal notation
 *
 * \param x is an ip address (32bit int)
 * \param y is the octet (1 - 4)
 * 1 is leftmost octet (assuming host byte order)
 * 4 is rightmost octet
 */
#define Oct(x,y) (((x) >> (8*(4-(y)))) & 0xFF)

#define NUMCONS 100          /* support 100 simaltaneous connections */
#define TTL 120              /* Time to live is 120 seconds */
#define PORTOFFSET 10000     /* port offset for each connection */

// contains a single record in the nattable
struct table_entry_str
{
	unsigned short _extport;	// external port assigned to the connection
	unsigned long _ttl;		// time when entry becomes invalid
	struct tuple4 _con;		// connection info	
};

// private data for the NATtable class
struct nattable_str
{
	struct table_entry_str _table[NUMCONS];
	unsigned int _ext_ip;
};

extern int pipelink[NUMPIPES][2];

static pthread_mutex_t mutex;

/** is_traddr_valid (private)
 *
 * checks the addresses to see if we want to translate the packet.
 * \param this NATtable object
 * \param src the source IP address
 * \param dst the destination IP address
 * \return 0 on bad address, 1 otherwise.
 */
static int is_traddr_valid(NATtable t, unsigned int src, unsigned int dst)
{
	if (src == t->_ext_ip ||
	    dst == t->_ext_ip ||
	    (Oct(dst,4) == 192 && Oct(dst,3) == 168) ||
	    !(Oct(src,4) == 192 && Oct(src,3) == 168))
		return 0;

	return 1;
}

/** is_utaddr_valid (private)
 *
 * checks the addresses to see if we want to translate the packet.
 * \param this  NATtable object
 * \param src the source IP address
 * \param dst the destination IP address
 * \return 0 on bad address, 1 otherwise.
 */
static int is_utaddr_valid(NATtable this, unsigned int src, unsigned int dst)
{
	if (src == this->_ext_ip ||
	    dst != this->_ext_ip)
		return 0;

	return 1;
}


/** findcon (private)
 *
 * do a nat lookup based on connection information
 * \param this NATtable object
 * \param in tuple4 containing the connection details to look up in the table
 * \param out to be filled with the nat entry associated with an untrused port (or 0 on failure)
 */
static void findcon(NATtable this, struct tuple4* in, struct tuple4* out)
{
	if (!is_utaddr_valid(this, in->saddr, in->daddr)) {
		memset(out,0,sizeof(struct tuple4));
		fprintf(stderr,"UNTRUSTED: INVALID address received.\n");
		return;
	}

	int i;
	for (i=0; i<NUMCONS; ++i) {
		pthread_mutex_lock(&mutex);
		if (this->_table[i]._extport == in->dest) {
			out->daddr = this->_table[i]._con.saddr;
			out->dest = this->_table[i]._con.source;
			out->saddr = this->_table[i]._con.daddr;
			out->source = this->_table[i]._con.dest;
			pthread_mutex_unlock(&mutex);
			return;
		}
		pthread_mutex_unlock(&mutex);
	}

	fprintf(stderr,"NO CONNECTION FOUND.\n");
	memset(out,0,sizeof(struct tuple4));
	return;
}

/** makeentry (private)
 *
 *  attempts to create/update an entry in the nat table 
 *  \param this NATtable object
 *  \param in contains src/dest IPs and Ports for entry into the table
 *  \param out will be filled with the translated connection (or 0 on failure)
 */
static void makeentry(NATtable this, struct tuple4* in, struct tuple4* out)
{
	if (!is_traddr_valid(this, in->saddr, in->daddr)) {
		fprintf(stderr,"TRUSTED: INVALID address received.\n");
		memset(out,0,sizeof(struct tuple4));
		return;
	}

	out->daddr = in->daddr;
	out->dest = in->dest;

	int vacant = -1;
	int ext_port = -1;
	int i;

	for (i=0; i<NUMCONS; ++i) {
		pthread_mutex_lock(&mutex);	
		if (this->_table[i]._con.saddr == in->saddr &&
		    this->_table[i]._con.source == in->source &&
		    this->_table[i]._con.daddr == in->daddr &&
		    this->_table[i]._con.dest == in->dest) {
			this->_table[i]._ttl = time(0) + TTL;
			out->saddr = this->_ext_ip;
			out->source = this->_table[i]._extport;
			pthread_mutex_unlock(&mutex);
			return;
		}
		if (vacant == -1 && (this->_table[i]._extport == 0 ||
				     this->_table[i]._ttl < time(0))) {
			vacant = i;
			ext_port = htons(PORTOFFSET + i);
		}
		pthread_mutex_unlock(&mutex);
	}

	if (vacant == -1) {
		memset(out,0,sizeof(struct tuple4));
		fprintf(stderr,"TRUSTED: Too many connections open. Discard packet.\n");
		return;
	}

	pthread_mutex_lock(&mutex);
	this->_table[vacant]._extport = ext_port;
	this->_table[vacant]._ttl = time(0) + TTL;
	this->_table[vacant]._con.saddr = in->saddr;
	this->_table[vacant]._con.source = in->source;
	this->_table[vacant]._con.daddr = in->daddr;
	this->_table[vacant]._con.dest = in->dest;
	out->saddr = this->_ext_ip;
	out->source = this->_table[vacant]._extport;

	printf("INFO: Connection assigned to port %i in table entry %i.\n",
	       ntohs(this->_table[vacant]._extport), vacant);
	pthread_mutex_unlock(&mutex);

	return;
}

// ctor
NATtable create_table(unsigned int ext_ip)
{
	int i;
	NATtable this;

	this = (NATtable) malloc(sizeof(struct nattable_str));

	if (this != NULL) {
		memset(&this->_table, 0 , sizeof(struct table_entry_str) * NUMCONS);
		
		this->_ext_ip = ext_ip;
		unsigned int saddr = ntohl(ext_ip);
		printf("Ext addr: %d.%d.%d.%d\n", Oct(saddr, 1),
			       Oct(saddr, 2), Oct(saddr, 3), Oct(saddr, 4));			
	}
	return this;
}

// trusted iface translator thread
void* translate_trust(void* this)
{
	int ret;
	struct tuple4 inpkg;
	struct tuple4 outpkg;
	NATtable t = (NATtable) this;

	printf("translate trust\n");
	while (1) {
		ret = read(pipelink[TROUT][READ],(void*)&inpkg,sizeof(struct tuple4));
		if (ret <= 0) {
			fprintf(stderr, "translate_trust failed to retrieve data\n");
			break;
		}
		printf("Trusted data received, now translating\n");		
		makeentry(t, &inpkg, &outpkg);
		write(pipelink[TRIN][WRITE],(void*)&outpkg,sizeof(struct tuple4));
	}
	printf("finish translate trust\n");

	return NULL;
}

// untrusted iface translator thread
void* translate_untrust(void* this)
{
	int ret;
	struct tuple4 indata;
	struct tuple4 outdata;
	NATtable t = (NATtable) this;

	printf("translate untrust\n");
	while (1) {
		ret = read(pipelink[UTOUT][READ],(void*)&indata,sizeof(struct tuple4));
		if (ret <= 0) {
			fprintf(stderr, "translate_untrust failed to retrieve data\n");
			break;
		}
		printf("Untrusted data received, now translating\n");
		findcon(t, &indata, &outdata);
		write(pipelink[UTIN][WRITE],(void*)&outdata,sizeof(struct tuple4));
	}
	printf("finish translate untrust\n");

	return NULL;
}

// dtor
void delete_table(NATtable this)
{
	free(this);
}
