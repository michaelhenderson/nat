/*

nat - Network Address Translator
Copyright (C) 2007  Michael Henderson and Valentin Koch

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA


*/
#include <stdlib.h>
#include <stdio.h>

#include "nat.h"
#include "trlistener.h"

// TRListener private data
struct trlistener_str
{
	Listener _listener;
};

// ctor
TRListener create_tr_listener()
{
	TRListener this;

	this = (TRListener) malloc(sizeof(struct trlistener_str));
	
	if (this != NULL) {
		this->_listener = create_listener();
		if (this->_listener != NULL) {

		}
	}
	return this;
}

// dtor
void delete_tr_listener(TRListener this)
{
	delete_listener(this->_listener);
	free(this);
}

// listener main function
void tr_listen(TRListener this, char* iface_in, char* iface_out)
{
	// call "parent" listener function
	listen_iface(this->_listener, iface_in, iface_out, (void*)this, TRIN, TROUT, "TRUSTED");
}

