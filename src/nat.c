/*

nat - Network Address Translator
Copyright (C) 2007  Michael Henderson and Valentin Koch

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA


*/
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#include "nids.h"
#include "libnet.h"
#include "nat.h"
#include "nattable.h"
#include "trlistener.h"
#include "utlistener.h"

int pipelink[NUMPIPES][2];

void usage()
{
	printf("\nUSAGE: nat [-t device] [-u device] [-e IP address]\n");
	printf("nat must be run with root privileges and assumes the internal network is 192.168.0.0\n\n");
	printf("Options:\n\n -t device \t set the trusted (internal) interface\n");
	printf(" -u device \t set the untrusted (external) interface\n");
	printf(" -e IP address \t set the external IP address\n\n");
}

int main(int argc, char* argv[])
{
	pthread_t tid, uid;

	char* tiface = "eth1";
	char* uiface = "eth0";
	char* addr = "142.231.69.144";
	struct in_addr ext_ip;
	int i;
	int c;

	/* process the cmd line arguments */
	while ((c = getopt (argc, argv, "t:u:e:h")) != -1) {
		switch (c) {
		
		case 't':
			tiface = optarg;
			break;
		case 'u':
			uiface = optarg;
			break;
		case 'e':
			addr = optarg;
			break;
		case 'h':
			usage();
			exit(0);
		default:
			abort ();
		}
	}

	if (!inet_aton(addr, &ext_ip)) {
		fprintf(stderr, "FATAL ERROR: Invalid external ip address: %s.\n", addr);
		exit(1);
	}

	for (i=0; i<NUMPIPES; ++i)
		pipe(pipelink[i]);

	if (fork() == 0) {		
		// We are the trust listener now
		
		// close unused pipe links
		close(pipelink[UTIN][READ]);
		close(pipelink[UTIN][WRITE]);
		close(pipelink[UTOUT][READ]);
		close(pipelink[UTOUT][WRITE]);
		close(pipelink[TRIN][WRITE]);
		close(pipelink[TROUT][READ]);
		
		TRListener tl = create_tr_listener();
		tr_listen(tl, tiface, uiface);
		delete_tr_listener(tl);		

		return 0;
	}

	if (fork() == 0) {		
		// We are the untrust listener now

		// close unused pipe links
		close(pipelink[TRIN][READ]);
		close(pipelink[TRIN][WRITE]);
		close(pipelink[TROUT][READ]);
		close(pipelink[TROUT][WRITE]);
		close(pipelink[UTIN][WRITE]);
		close(pipelink[UTOUT][READ]);

		UTListener ul = create_ut_listener();
		ut_listen(ul, uiface, tiface);
		delete_ut_listener(ul);

		return 0;
	}

	// We are the nat database

	// close unused pipe links
	close(pipelink[TRIN][READ]);
	close(pipelink[TROUT][WRITE]);
	close(pipelink[UTIN][READ]);
	close(pipelink[UTOUT][WRITE]);
	
	NATtable table = create_table(ext_ip.s_addr);
	pthread_create(&tid, NULL, translate_trust, (void*)table);
	pthread_create(&uid, NULL, translate_untrust, (void*)table);

	(void) pthread_join(tid, NULL);
	(void) pthread_join(uid, NULL);

	delete_table(table);
}
